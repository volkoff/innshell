#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h>
#ifdef WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <signal.h>
#include "libinnshell.h"
#include "uci.h"

/* max number of characters that escaping adds to the string */
#define UCI_QUOTE_ESCAPE	"'\\''"
#define LINEBUF	32
#define LINEBUF_MAX	4096

#define CLITEST_PORT                8000
#ifdef __GNUC__
# define UNUSED(d) d __attribute__ ((unused))
#else
# define UNUSED(d) d
#endif

unsigned int regular_count = 0;
unsigned int debug_regular = 0;

//volkoff: UCI defs 
static void parse_uci_config(int argc, char **argv);
static struct uci_context *ctx;
static const char *delimiter = ", ";
int uci_export_cli(struct uci_context *ctx, struct uci_package *package, bool header);
int uci_export_file(struct uci_context *ctx, char *filename, const char* mode, struct uci_package *package, bool header);
//ivan_k: shell config
char* confdir = NULL;
int check_file(const char *path);

enum {
	/* section cmds */
	CMD_GET,
	CMD_SET,
	//CMD_ADD_LIST,
	//CMD_DEL,
	//CMD_RENAME,
	CMD_REVERT,
	//CMD_REORDER,
	/* package cmds */
	CMD_SHOW,
	CMD_CHANGES,
	CMD_EXPORT,
	CMD_COMMIT,
	/* other cmds */
	//CMD_ADD,
	CMD_IMPORT,
	//CMD_HELP,
};


struct my_context {
  int value;
  char* message;
};

#ifdef WIN32
typedef int socklen_t;

int winsock_init()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    // Start up sockets
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0)
    {
        // Tell the user that we could not find a usable WinSock DLL.
        return 0;
    }

    /*
     * Confirm that the WinSock DLL supports 2.2
     * Note that if the DLL supports versions greater than 2.2 in addition to
     * 2.2, it will still return 2.2 in wVersion since that is the version we
     * requested.
     * */
    if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2)
    {
        // Tell the user that we could not find a usable WinSock DLL.
        WSACleanup();
        return 0;
    }
    return 1;
}
#endif

int cmd_uci_exec_wrapper(struct cli_def *cli, const char *command, char *argv[], int argc);

int cmd_test(struct cli_def *cli, const char *command, char *argv[], int argc)
{
    int i;
    cli_print(cli, "called %s with \"%s\"", __FUNCTION__, command);
    cli_print(cli, "%d arguments:", argc);
    for (i = 0; i < argc; i++)
        cli_print(cli, "        %s", argv[i]);

    return CLI_OK;
}

int cmd_set(struct cli_def *cli, UNUSED(const char *command), char *argv[],
    int argc)
{
    if (argc < 2 || strcmp(argv[0], "?") == 0)
    {
        cli_print(cli, "Specify a variable to set");
        return CLI_OK;
    }

    if (strcmp(argv[1], "?") == 0)
    {
        cli_print(cli, "Specify a value");
        return CLI_OK;
    }

    if (strcmp(argv[0], "regular_interval") == 0)
    {
        unsigned int sec = 0;
        if (!argv[1] && !&argv[1])
        {
            cli_print(cli, "Specify a regular callback interval in seconds");
            return CLI_OK;
        }
        sscanf(argv[1], "%u", &sec);
        if (sec < 1)
        {
            cli_print(cli, "Specify a regular callback interval in seconds");
            return CLI_OK;
        }
        cli->timeout_tm.tv_sec = sec;
        cli->timeout_tm.tv_usec = 0;
        cli_print(cli, "Regular callback interval is now %d seconds", sec);
        return CLI_OK;
    }

    cli_print(cli, "Setting \"%s\" to \"%s\"", argv[0], argv[1]);
    return CLI_OK;
}

int cmd_config_int(struct cli_def *cli, UNUSED(const char *command), char *argv[], int argc)
{
    if (argc < 1)
    {
        cli_print(cli, "Specify an interface to configure");
        return CLI_OK;
    }

    if (strcmp(argv[0], "?") == 0)
        cli_print(cli, "  test0/0");

    else if (strcasecmp(argv[0], "test0/0") == 0)
        cli_set_configmode(cli, MODE_CONFIG, "test");
    else
        cli_print(cli, "Unknown interface %s", argv[0]);

    return CLI_OK;
}

int cmd_config_int_exit(struct cli_def *cli, UNUSED(const char *command), UNUSED(char *argv[]), UNUSED(int argc))
{
    cli_set_configmode(cli, MODE_CONFIG, NULL);
    return CLI_OK;
}

int cmd_show_regular(struct cli_def *cli, UNUSED(const char *command), char *argv[], int argc)
{
    cli_print(cli, "cli_regular() has run %u times", regular_count);
    return CLI_OK;
}

int cmd_debug_regular(struct cli_def *cli, UNUSED(const char *command), char *argv[], int argc)
{
    debug_regular = !debug_regular;
    cli_print(cli, "cli_regular() debugging is %s", debug_regular ? "enabled" : "disabled");
    return CLI_OK;
}

int cmd_context(struct cli_def *cli, UNUSED(const char *command), UNUSED(char *argv[]), UNUSED(int argc))
{
    struct my_context *myctx = (struct my_context *)cli_get_context(cli);
    cli_print(cli, "User context has a value of %d and message saying %s", myctx->value, myctx->message);
    return CLI_OK;
}

int check_auth(const char *username, const char *password)
{
//    if (strcasecmp(username, "fred") != 0)
//        return CLI_ERROR;
//    if (strcasecmp(password, "nerk") != 0)
//        return CLI_ERROR;
    return CLI_OK;
}

int regular_callback(struct cli_def *cli)
{
    regular_count++;
    if (debug_regular)
    {
        cli_print(cli, "Regular callback - %u times so far", regular_count);
        cli_reprompt(cli);
    }
    return CLI_OK;
}

int check_enable(const char *password)
{
    return !strcasecmp(password, "topsecret");
}

int idle_timeout(struct cli_def *cli)
{
    cli_print(cli, "Custom idle timeout");
    return CLI_QUIT;
}

void pc(UNUSED(struct cli_def *cli), const char *string)
{
    printf("%s\n", string);
}

static  struct cli_def *cli;

int main(int argc, char **argv)
{
    struct cli_command *c;
  
    int s, x;
    struct sockaddr_in addr;
    int on = 1;

#ifndef WIN32
    signal(SIGCHLD, SIG_IGN);
#endif
#ifdef WIN32
    if (!winsock_init()) {
        printf("Error initialising winsock\n");
        return 1;
    }
#endif

    // Prepare a small user context
    char mymessage[] = "I contain user data!";
    struct my_context myctx;
    myctx.value = 5;
    myctx.message = mymessage;

    cli = cli_init();
    cli_set_banner(cli, "Wellcome to Innbox Shell\nCisco-style CLI for UCI");
    cli_set_hostname(cli, "Innbox");
    cli_telnet_protocol(cli, 1);
    cli_regular(cli, regular_callback);
    cli_regular_interval(cli, 5); // Defaults to 1 second
    cli_set_idle_timeout_callback(cli, 60, idle_timeout); // 60 second idle timeout
    /* Ivan_k disable commands
    cli_register_command(cli, NULL, "test", cmd_test, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, NULL);


    cli_register_command(cli, NULL, "set", cmd_set, PRIVILEGE_PRIVILEGED, MODE_EXEC, NULL);

    c = cli_register_command(cli, NULL, "debug", NULL, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, NULL);

    cli_register_command(cli, c, "regular", cmd_debug_regular, PRIVILEGE_UNPRIVILEGED, MODE_EXEC,
                         "Enable callback for debugging");
    */

   

 //   cli_set_auth_callback(cli, check_auth);
    cli_set_enable_callback(cli, check_enable);

    // for Rok: this demo shows how to add commands from a text file
    {
        FILE *fh;

        if ((fh = fopen("clitest.txt", "r")))
        {
            // This sets a callback which just displays the cli_print() text to stdout
            cli_print_callback(cli, pc);
            cli_file(cli, fh, PRIVILEGE_UNPRIVILEGED, MODE_EXEC);
            cli_print_callback(cli, NULL);
            fclose(fh);
        }
    }

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        return 1;
    }
    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(CLITEST_PORT);
    if (bind(s, (struct sockaddr *) &addr, sizeof(addr)) < 0)
    {
        perror("bind");
        return 1;
    }

    if (listen(s, 50) < 0)
    {
        perror("listen");
        return 1;
    }
    // volkoff: Parse UCI!
    // ivan_k: Don't parse UCI here! We need each command been executed in its own uci context
    parse_uci_config(argc, argv);

    printf("Listening on port %d\n", CLITEST_PORT);
    while ((x = accept(s, NULL, 0)))
    {
#ifndef WIN32
        int pid = fork();
        if (pid < 0)
        {
            perror("fork");
            return 1;
        }

        /* parent */
        if (pid > 0)
        {
            socklen_t len = sizeof(addr);
            if (getpeername(x, (struct sockaddr *) &addr, &len) >= 0)
                printf(" * accepted connection from %s\n", inet_ntoa(addr.sin_addr));

            close(x);
            continue;
        }

        /* child */
        close(s);
        cli_loop(cli, x);
        exit(0);
#else
        cli_loop(cli, x);
        shutdown(x, SD_BOTH);
        close(x);
#endif
    }
   //free UCI context
   //ivan_k: not here!
   //uci_free_context(ctx);
    cli_done(cli);
    return 0;
}




//by volkov: uci parsing


//static const char *appname;

static enum {
	CLI_FLAG_MERGE =    (1 << 0),
	CLI_FLAG_QUIET =    (1 << 1),
	CLI_FLAG_NOCOMMIT = (1 << 2),
	CLI_FLAG_BATCH =    (1 << 3),
	CLI_FLAG_SHOW_EXT = (1 << 4),
	CLI_FLAG_NOPLUGINS= (1 << 5),
} flags;

struct uci_type_list {
	unsigned int idx;
	const char *name;
	struct uci_type_list *next;
};

static struct uci_type_list *type_list = NULL;
static char *typestr = NULL;


static const char *cur_section_ref;

static void uci_reset_typelist(void)
{
	struct uci_type_list *type;
	while (type_list != NULL) {
			type = type_list;
			type_list = type_list->next;
			free(type);
	}
	if (typestr) {
		free(typestr);
		typestr = NULL;
	}
	cur_section_ref = NULL;
}

//ivan_k: function for concatinating argvs to uci format and checking arguments number for each uci command
int check_params(int cmd, char **argv, int argc, char** param)
{
	size_t params_len = 0;
	int i;
	for (i = 0; i<argc; i++)
	  params_len += strlen(argv[i]) +1;

	*param = (char*) malloc(params_len);

	switch(cmd) {
		case CMD_SET: //3 or 4 - <config>.<section>[.<option>]=<value>
			if (argc == 3 || argc == 4)
			{
				sprintf(*param, "%s.%s", argv[0],argv[1]);
				if (argc == 3)
				{
				  strcat(*param, "=");
				  strcat(*param, argv[2]);
				}
				else
				{
				 strcat(*param, ".");
				 strcat(*param, argv[2]);
				 strcat(*param, "=");
				 strcat(*param, argv[3]);
				}
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: set <config> <section> [<option>] <value>");
				return CLI_ERROR;
			}
			break;
		case CMD_GET: //2 or 3 - <config>.<section>[.<option>] 
			if (argc == 2 || argc == 3)
			{
				sprintf(*param, "%s", argv[0]);
				for (i = 1; i < argc; i++)
				{
				 strcat(*param, ".");
				 strcat(*param, argv[i]);
				}
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: get <config> <section> [<option>]");
				return CLI_ERROR;
			}
			break;
		case CMD_SHOW: //0 or 1 or 2 or 3 - [<config>[.<section>[.<option>]]]
			if (argc >= 0 && argc <= 3)
			{
				if (argc > 0)
				{
					sprintf(*param, "%s", argv[0]);
					for (i = 1; i < argc; i++)
					{
					 strcat(*param, ".");
					 strcat(*param, argv[i]);
					}
				}
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: show [<config> [<section> [<option>]]]");
				return CLI_ERROR;
			}
			break;
		case CMD_CHANGES: //0 or 1 - [<config>]
			if (argc == 0 || argc == 1)
			{
				if (argc == 1)
					sprintf(*param, "%s", argv[0]);
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: changes [<config>]");
				return CLI_ERROR;
			}
			break;
		case CMD_COMMIT: //0 or 1 - [<config>]
			if (argc == 0 || argc == 1)
			{
				if (argc == 1)
					sprintf(*param, "%s", argv[0]);
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: commit [<config>]");
				return CLI_ERROR;
			}
			break;
		case CMD_REVERT: //1 or 2 or 3 - <config>[.<section>[.<option>]]
			if (argc >= 1 && argc <= 3)
			{
				sprintf(*param, "%s", argv[0]);
				for (i = 1; i < argc; i++)
				{
				 strcat(*param, ".");
				 strcat(*param, argv[i]);
				}
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: revert <config>[.<section>[.<option>]]");
				return CLI_ERROR;
			}
			break;
		case CMD_EXPORT: //[config] [> filename]
			if (argc == 0 || argc == 1 || (argc == 3 && strcmp(argv[1], ">") == 0) || (argc == 2 && strcmp(argv[0], ">") == 0))
			{
				//cli_print(cli, "export valid params");
				if (argc == 1 || argc == 3)
				{
					sprintf(*param, "%s", argv[0]);
				}
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: export [<config>] [> file]");
				return CLI_ERROR;				
			}
		case CMD_IMPORT: //[config] [> filename]
			if ((argc == 2 || argc == 3) && strcmp(argv[argc - 1], "<") != 0)
			{
				//cli_print(cli, "import valid params");
				if (argc == 3)
				{
					sprintf(*param, "%s", argv[0]);
				}
				return CLI_OK;
			}
			else
			{
				cli_print(cli, "Usage: import [<config>] < file");
				return CLI_ERROR;				
			}
		default:
			/* should not happen */
			return CLI_ERROR;
	}
	return CLI_ERROR;
}

static char * uci_lookup_section_ref(struct uci_section *s)
{
	struct uci_type_list *ti = type_list;
	int maxlen;

	if (!s->anonymous || !(flags & CLI_FLAG_SHOW_EXT))
		return s->e.name;

	/* look up in section type list */
	while (ti) {
		if (strcmp(ti->name, s->type) == 0)
			break;
		ti = ti->next;
	}
	if (!ti) {
		ti = malloc(sizeof(struct uci_type_list));
		if (!ti)
			return NULL;
		memset(ti, 0, sizeof(struct uci_type_list));
		ti->next = type_list;
		type_list = ti;
		ti->name = s->type;
	}

	maxlen = strlen(s->type) + 1 + 2 + 10;
	if (!typestr) {
		typestr = malloc(maxlen);
	} else {
		typestr = realloc(typestr, maxlen);
	}

	if (typestr)
		sprintf(typestr, "@%s[%d]", ti->name, ti->idx);

	ti->idx++;

	return typestr;
}

static void uci_show_value(struct uci_option *o)
{
	struct uci_element *e;
	bool sep = false;

	switch(o->type) {
	case UCI_TYPE_STRING:
		cli_print(cli,"%s", o->v.string);
		break;
	case UCI_TYPE_LIST:
		uci_foreach_element(&o->v.list, e) {
			cli_print_nonl(cli,"%s%s", (sep ? delimiter : ""), e->name);
			sep = true;
		}
		cli_print(cli,"\r");
		break;
	default:
		cli_print(cli,"<unknown>");
		break;
	}
}

static void uci_show_option(bool show_parent, struct uci_option *o)
{
	if (show_parent)
		cli_print_nonl(cli,"%s %s %s ",
			o->section->package->e.name,
			(cur_section_ref ? cur_section_ref : o->section->e.name),
			o->e.name);
	else
		cli_print_nonl(cli,"    %s=",	o->e.name);

	uci_show_value(o);
}

static void uci_show_section(bool show_parent, struct uci_section *s)
{
	struct uci_element *e;
	cur_section_ref = uci_lookup_section_ref(s);
        if (show_parent)
		cli_print(cli,"%s.%s: %s", s->package->e.name,
			(cur_section_ref ? cur_section_ref : s->e.name), s->type);
	else
		cli_print(cli,"  %s: %s", (cur_section_ref ? cur_section_ref : s->e.name), s->type);
	uci_foreach_element(&s->options, e) {
		uci_show_option(false, uci_to_option(e));
	}
	cli_print(cli,"\r");
}


static void uci_show_package(struct uci_package *p)
{
	struct uci_element *e;
	cli_print(cli,"%s:\n", p->e.name);
 	uci_reset_typelist();
	uci_foreach_element( &p->sections, e) {
		uci_show_section(false, uci_to_section(e));
	}
	uci_reset_typelist();
}

static void uci_show_changes(struct uci_package *p)
{
	struct uci_element *e;

	uci_foreach_element(&p->saved_delta, e) {
		struct uci_delta *h = uci_to_delta(e);
		char prefix[4] = "";
		char op[4] = "=";
		switch(h->cmd) {
		case UCI_CMD_REMOVE:
			strcpy(prefix,"-");
			break;
		case UCI_CMD_LIST_ADD:
			strcpy(op,"+=");
			break;
		default:
			break;
		}
		cli_print(cli, "%s%s %s", prefix, p->e.name, h->section);
		if (e->name)
			cli_print(cli, "%s", e->name);
		if (h->cmd != UCI_CMD_REMOVE)
			cli_print(cli, "%s%s", op, h->value);
		cli_print(cli,"\n");
	}
}


//ivan_k: all uci command implementation :)
int cmd_uci_exec_wrapper(struct cli_def *cli, const char *command, char *argv[], int argc)
{
	ctx = uci_alloc_context();
	if (!ctx) {
		printf("Out of memory\n");
		return CLI_ERROR;
	}
	uci_set_confdir(ctx, confdir);
	cmd_uci_exec(cli, command, argv, argc);
	uci_free_context(ctx);
}

int cmd_uci_exec(struct cli_def *cli, const char *command, char *argv[], int argc)
{
	struct uci_ptr ptr;
	char **configs = NULL;
	char **p;
	int cmd = 0;
	char *param = NULL;
	int secNum = 0;
	
	FILE * from;
	struct uci_package *package = NULL;
	char *name = NULL;
	int ret = UCI_OK;
	bool merge = false;

	if (!strcasecmp(command, "show"))
	{
		cmd = CMD_SHOW;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{
			if (argc == 0) // show whole system config
			{
				cli_print(cli, "Whole system configuration:\n");

				if ((uci_list_configs(ctx, &configs) != UCI_OK) || !configs)
				{
					cli_print(cli, "Configuration is empty?");
					return CLI_OK;
				}

				for (p = configs; *p; p++)
				{
					if (uci_lookup_ptr(ctx, &ptr, *p, true) != UCI_OK )
					{
						cli_print(cli, "Module \"%s\" is invalid?", *p);
						continue;
					}
					uci_show_package(ptr.p);
				}
				return CLI_OK;
			}
			if (uci_lookup_ptr(ctx, &ptr, param, true) != UCI_OK)
			{
				cli_print(cli, "\"%s\" not found.", param);
				return CLI_ERROR;
			}
			else
			{
				if (!(ptr.flags & UCI_LOOKUP_COMPLETE))
				{
					ctx->err = UCI_ERR_NOTFOUND;
					cli_print(cli,"Section \"%s %s\" not found.", argv[0], argv[1]);
					return CLI_ERROR;
				}
				switch(ptr.last->type) {
					case UCI_TYPE_PACKAGE:
						uci_show_package(ptr.p);
						break;
					case UCI_TYPE_SECTION:
						uci_show_section(true, ptr.s);
						break;
					case UCI_TYPE_OPTION:
						uci_show_option(true, ptr.o);
						break;
					default:
						/* should not happen */
						return 1;
				}
				return CLI_OK;
			}
		}
		else
			return CLI_ERROR;
	}
	else if (!strcasecmp(command, "changes"))
	{
		cmd = CMD_CHANGES;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{
			if (argc == 1) //changes [<config>]
			{
				if (uci_lookup_ptr(ctx, &ptr, param, true) != UCI_OK)
				{
					cli_print(cli, "Section \"%s\" not found.", param);
					return CLI_ERROR;
				}
				else
				{
					if (!(ptr.flags & UCI_LOOKUP_COMPLETE))
					{
						ctx->err = UCI_ERR_NOTFOUND;
						cli_print(cli,"Section \"%s\" not found.", argv[0]);
						return CLI_ERROR;
					}
					uci_show_changes(ptr.p);
					return CLI_OK;
				}
			}
			if ((uci_list_configs(ctx, &configs) != UCI_OK) || !configs)
			{
				cli_print(cli, "Error reading config.");
				return CLI_ERROR;
			}
			for (p = configs; *p; p++)
			{
				if (uci_lookup_ptr(ctx, &ptr, *p, true) != UCI_OK)
				{
					cli_print(cli, "\"%s\" not found.", *p);
					continue;
				}
				else
					uci_show_changes(ptr.p);
			}
		}
	}	
	else if (!strcasecmp(command, "commit"))
	{
		cmd = CMD_COMMIT;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{
			if (argc == 0)
			{
				if ((uci_list_configs(ctx, &configs) != UCI_OK) || !configs)
				{
					cli_print(cli, "Configuration is empty?");
					return CLI_OK;
				}

				for (p = configs; *p; p++)
				{
					if (uci_lookup_ptr(ctx, &ptr, *p, true) != UCI_OK )
					{
						cli_print(cli, "Module \"%s\" is invalid?", *p);
						continue;
					}

					if (uci_commit(ctx, &ptr.p, false) != UCI_OK)
					{
						cli_print(cli, "Can't commit changes.");
					}
					else
					{
						cli_print(cli, "Commiting \"%s\" done.", *p);
					}
				}
				return CLI_OK;
			}
			else
			{
				if (uci_lookup_ptr(ctx, &ptr, param, true) != UCI_OK )
				{
					cli_print(cli, "Module \"%s\" is invalid?", param);
					return CLI_ERROR;
				}

				if (uci_commit(ctx, &ptr.p, false) != UCI_OK)
				{
					cli_print(cli, "Can't commit changes.");
					return CLI_ERROR;
				}
				else
				{
					cli_print(cli, "Commiting \"%s\" done.", param);
					return CLI_OK;
				}
			}
		}
		else
			return CLI_ERROR;
	}
	else if (!strcasecmp(command, "get"))
	{
		cmd = CMD_GET;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{
			if (uci_lookup_ptr(ctx, &ptr, param, true) != UCI_OK)
			{
				cli_print(cli, "\"%s\" not found.", param);
				return CLI_ERROR;
			}
			else
			{
				if (!(ptr.flags & UCI_LOOKUP_COMPLETE))
				{
					  ctx->err = UCI_ERR_NOTFOUND;
					  cli_print(cli,"Section \"%s %s\" not found.", argv[0], argv[1]);
					  return CLI_ERROR;
				}

				switch(ptr.last->type)
				{
					case UCI_TYPE_SECTION:
						cli_print(cli, "%s", ptr.s->type);
						break;
					case UCI_TYPE_OPTION:
						uci_show_value(ptr.o);
						break;
					default:
						return CLI_ERROR;
					break;
				}
				return CLI_OK;
			}
		}
		else
			return CLI_ERROR;
	}
	else if (!strcasecmp(command, "set") || !strcasecmp(command,"configure"))
	{
		cmd = CMD_SET;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{
			if (uci_lookup_ptr(ctx, &ptr, param, true) != UCI_OK)
			{
				cli_print(cli, "\"%s\" not found.", param);
				return CLI_ERROR;
			}
			else
			{//we need to check if section and module founded, otherwise command works as add/insert
				if (uci_set(ctx, &ptr) == UCI_OK)
				{
					if (uci_save(ctx, ptr.p) != UCI_OK) {
						cli_print(cli, "Can't save changes.");
						return CLI_ERROR;
					}
					else
					{
						cli_print(cli, "Done!");
						return CLI_OK;
					}
				}
			}
		}
	}
	else if (!strcasecmp(command, "revert"))
	{
		cmd = CMD_REVERT;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{
			if (uci_lookup_ptr(ctx, &ptr, param, true) != UCI_OK)
			{
				cli_print(cli, "\"%s\" not found.", param);
				return CLI_ERROR;
			}
			else
			{
				if (!(ptr.flags & UCI_LOOKUP_COMPLETE))
				{
					  ctx->err = UCI_ERR_NOTFOUND;
					  cli_print(cli, "\"%s\" not found.", param);
					  return CLI_ERROR;
				}
				if (uci_revert(ctx, &ptr) == UCI_OK)
				{
					cli_print(cli, "Done!");
					return CLI_OK;
				}
				else
				{
					cli_print(cli, "Can not revert changes.");
					return CLI_ERROR;
				}
			}
		}
		else
			return CLI_ERROR;
	}
	else if (!strcasecmp(command, "export"))
	{
		cmd = CMD_EXPORT;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{
			if (argc == 0 || argc == 2) // export whole system config to STDIO or file
			{
				cli_print(cli, "Exporting whole system configuration.\n");

				if ((uci_list_configs(ctx, &configs) != UCI_OK) || !configs)
				{
					cli_print(cli, "Configuration is empty?");
					return CLI_OK;
				}
				

				for (p = configs; *p; p++)
				{
					if (uci_lookup_ptr(ctx, &ptr, *p, true) != UCI_OK )
					{
						cli_print(cli, "Module \"%s\" is invalid?", *p);
						continue;
					}
					if (argc == 2)
					{//file for output
						uci_export_file(ctx, argv[argc - 1], secNum > 0 ? "a+" : "w+", ptr.p, true);					
					}
					else
					{//tempfile
						uci_export_cli(ctx, ptr.p, true);
					}						
					secNum++;
				}
				cli_print(cli, "Done!");
				return CLI_OK;
			}
			if (uci_lookup_ptr(ctx, &ptr, param, true) != UCI_OK)
			{
				cli_print(cli, "\"%s\" not found.", param);
				return CLI_ERROR;
			}
			else
			{
				if (!(ptr.flags & UCI_LOOKUP_COMPLETE))
				{
					  ctx->err = UCI_ERR_NOTFOUND;
					  cli_print(cli, "\"%s\" not found.", param);
					  return CLI_ERROR;
				}
				FILE * fileBuf;
				if (argc == 3)
				{//file for output
					uci_export_file(ctx, argv[argc - 1], "w+", ptr.p, true);					
				}
				else
				{//tempfile
					uci_export_cli(ctx, ptr.p, true);
				}
				cli_print(cli, "Done!");
			}
		}
		else
		{
			cli_print(cli, "invalid params");
			return CLI_ERROR;
		}
	}
	else if (!strcasecmp(command, "import"))
	{
		cmd = CMD_IMPORT;
		if (check_params(cmd, argv, argc, &param) == CLI_OK)
		{			
			from = fopen(argv[argc -1],"r");
		
			if (argc == 3)
				name = argv[0];
			else if (flags & CLI_FLAG_MERGE)
				/* need a package to merge */
				return 255;

			if (flags & CLI_FLAG_MERGE) {
				if (uci_load(ctx, name, &package) != UCI_OK)
					package = NULL;
				else
					merge = true;
			}
			ret = uci_import(ctx, from, name, &package, (name != NULL));
			if (ret == UCI_OK) {
				if (merge) {
					ret = uci_save(ctx, package);
				} else {
					struct uci_element *e;
					/* loop through all config sections and overwrite existing data */
					uci_foreach_element(&ctx->root, e) {
						struct uci_package *p = uci_to_package(e);
						ret = uci_commit(ctx, &p, true);
					}
				}
			}

			if (ret != UCI_OK)
			{
				cli_print(cli, "Error!");
				return CLI_ERROR;
			}
			else
			{
				cli_print(cli, "Done!");
				return CLI_OK;
			}
		}
		else
		{
			cli_print(cli, "invalid params");
			return CLI_ERROR;
		}
	}
	return CLI_ERROR;
}

/* Check for regular file. */
int check_file(const char *path) {
	struct stat sb;
	return stat(path, &sb) == 0 && S_ISREG(sb.st_mode);
}

int uci_export_cli(struct uci_context *ctx, struct uci_package *package, bool header)
{
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	FILE *fileBuf;
	fileBuf = tmpfile();
	if (uci_export(ctx, fileBuf, package, header) == UCI_OK)
	{
		rewind (fileBuf);
		while ((read = getline(&line, &len, fileBuf)) != -1) 
		{
			chomp(line);
			cli_print(cli, "%s", line);							
		}

		if (line)  free(line);
		fclose(fileBuf);
		return CLI_OK;
	}
	else
	{
		cli_print(cli, "Can not export.");
		fclose(fileBuf);
		return CLI_ERROR;
	}
}

int uci_export_file(struct uci_context *ctx, char *filename, const char* mode, struct uci_package *package, bool header)
{
	FILE *fileBuf;
	fileBuf = fopen(filename, mode);
	if (uci_export(ctx, fileBuf, package, header) == UCI_OK)
	{
		fclose(fileBuf);
		return CLI_OK;
	}
	else
	{
		cli_print(cli, "Can not export.");
		fclose(fileBuf);
		return CLI_ERROR;
	}
}


static void uci_build_options(struct cli_command *parent, struct uci_option *o)
{
     cli_register_command(cli, parent, o->e.name, NULL, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, NULL);
}

static void uci_build_sections(struct cli_command *parent, struct uci_section *s)
{
	struct uci_element *e;
        cur_section_ref = uci_lookup_section_ref(s);
       	struct cli_command *c = cli_register_command(cli,
 			 parent, (cur_section_ref ? cur_section_ref : s->e.name), NULL, 						PRIVILEGE_UNPRIVILEGED, MODE_EXEC, NULL);

	uci_foreach_element(&s->options, e) {
		uci_build_options(c, uci_to_option(e));
	}
}

//volkoff: build command tree(s)
static void build_uci_cmd_tree(struct cli_command *parent, struct uci_package *p)
{
	printf("Processing %s", p->e.name);
	struct uci_element *e;
 
       	struct cli_command *c = cli_register_command(cli,
 			 parent, p->e.name, NULL, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, NULL);

 	uci_reset_typelist();
	uci_foreach_element( &p->sections, e) {
		uci_build_sections(c, uci_to_section(e));
	}
	uci_reset_typelist();
}


//volkoff: parse UCI config 
static void parse_uci_config(int argc, char **argv)
{
	struct uci_context *lctx;
	struct uci_ptr lptr;
    char **configs = NULL;
	char **p;
	int c;

//allocate context
	//ivan_k: not now
	
	lctx = uci_alloc_context();
	if (!lctx) {
		printf("Out of memory\n");
		return;
	}
	
//set config directory

	while((c = getopt(argc, argv, "c:d:f:LmnNp:P:sSqX")) != -1)  { 
		switch(c) {
			case 'c':
				//uci_set_confdir(ctx, optarg);
				confdir = malloc(strlen(optarg) + 1);
				sprintf(confdir, "%s", optarg);
				uci_set_confdir(lctx, confdir);
				printf ("Using UCI configuration from %s\n", optarg);
				break;
		default:
			//uci_set_confdir(ctx, "/etc/config");
			confdir = malloc(strlen("/etc/config") + 1);			
			sprintf(confdir, "/etc/config");
			uci_set_confdir(lctx, confdir);
			break;
		}
	}		
 
//add root  commands  (in commands tree)

//for Rok: adding only 5 hard-coded  root UCI commands.
//TODO: HELP will be taken from schema.

	
	
	 struct cli_command* uci_show = cli_register_command(cli, NULL, "show",
    			cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Show configuration");

    	struct cli_command* uci_set = cli_register_command(cli, NULL, "set",
    		cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Set configuration");
    	struct cli_command* uci_configure = cli_register_command(cli, NULL, "configure",
    		cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Set configuration");

    	struct cli_command* uci_get = cli_register_command(cli, NULL, "get",
    		cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Get configuration value");

    	struct cli_command* uci_revert = cli_register_command(cli, NULL, "revert",
    		cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Revert configuration changes");

    	struct cli_command* uci_commit = cli_register_command(cli, NULL, "commit",
    			cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Commit configuration changes");

    	struct cli_command* uci_changes = cli_register_command(cli, NULL, "changes",
    		cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Show configuration changes");
	
    	struct cli_command* uci_export = cli_register_command(cli, NULL, "export",
    		cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Export configuration");
		
		struct cli_command* uci_import = cli_register_command(cli, NULL, "import",
		cmd_uci_exec_wrapper, PRIVILEGE_UNPRIVILEGED, MODE_EXEC, "Import configuration");
	

//go through all the packages 
	if ((uci_list_configs(lctx, &configs) != UCI_OK) || !configs) {
		printf("Configuration is empty?\n");
		return;
	}

	for (p = configs; *p; p++) {
 		if (uci_lookup_ptr(lctx, &lptr, *p, true) != UCI_OK) {
			printf("UCI package %s is invalid?\n", *p);
			continue;
		}
		//HERE WE GO!
		build_uci_cmd_tree(uci_show, lptr.p); 
		build_uci_cmd_tree(uci_get, lptr.p); 
		build_uci_cmd_tree(uci_set, lptr.p); 
		build_uci_cmd_tree(uci_changes, lptr.p);
		build_uci_cmd_tree(uci_revert, lptr.p);
		build_uci_cmd_tree(uci_commit, lptr.p);
		build_uci_cmd_tree(uci_export, lptr.p);
		build_uci_cmd_tree(uci_import, lptr.p);
	}
	
uci_free_context(lctx);

}

